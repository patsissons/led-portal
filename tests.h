#ifndef TESTS_H
#define TESTS_H

#include <FastLED.h>

#ifdef __NEO_GRBW__
#include <Adafruit_NeoPixel.h>
#endif

// show all permutations of primary colors
void rgb_test() {
  static uint8_t frame;
  static uint32_t colors[] = { 
    0x00000000,
    0x00FF0000,
    0x0000FF00,
    0x000000FF,
    0x00FFFF00,
    0x0000FFFF,
    0x00FF00FF,
    0x00FFFFFF,
  };
  
  fill_solid(CRGB(colors[frame++ % 8]));

  show(1000);
}

// show all hues at full saturation and intensity
void hue_test() {
  static uint8_t hue;

  fill_solid(CHSV(hue++, 255, 255));

  fps_delay();
}

// show all hues with snaking saturation and intensity
// basically roll through the hue and inc/dec saturation and intensity for each hue roll
// i.e., desat, reduce intensity, resat, reduce intensity, ...
//       once we reach min intensity the process reverses
void hsv_test() {
  const uint8_t MinSat = 32, MinVal = 32;
  static CHSV color = CHSV(0, 255, 255);
  static int8_t ds = -32, dv = -32;

  fill_solid(color);

  // only adjust saturation or intensity at the end of a hue roll
  if (++color.h == 0) {
    // adjust saturation
    color.s += ds;

    if (color.s <= MinSat) {
      // reverse saturation
      ds *= -1;
      // undo the last saturation change
      color.s += ds;

      // adjust intensity
      color.v += dv;
    }

    if (color.v <= MinVal) {
      // reverse intensity
      dv *= -1;
      // undo the last intensity change
      color.v += dv;
    }
  }

  // we need to delay for a very small duration here because 
  // this test needs a LOT of frames to run through a full loop
  show(1);
}

void fill_solid_range_test() {
  static uint8_t frame, hue;
  static uint16_t offset;

  clear();
  
  fill_solid(CHSV(hue++, 255, 255), 5, offset);

  offset = incr(offset, ++frame, 32);

  fps_delay(60);
}

void fill_rainbow_range_test() {
  static uint8_t frame, hue;
  static uint16_t offset;

  clear();
  
  fill_rainbow(hue++, 7, offset);

  offset = incr(offset, ++frame, 32);

  fps_delay();
}

void fill_range_test() {
  static CRGBSet subset = static_cast<CRGBSet>(CRGBArray<5>());
  static uint8_t frame, hue;
  static uint16_t offset;

  clear();

  subset.fill_rainbow(hue++);

  fill_range(subset, offset);

  offset = incr(offset, ++frame, 8);

  fps_delay();
}

void mask_test() {
  static uint16_t offset;
  
  fill_solid(CRGB::Red);

  mask(4, offset);

  offset = incr(offset);

  fps_delay(15);
}

void invmask_test() {
  static uint16_t offset;
  
  fill_solid(CRGB::Red);

  invmask(4, offset);

  offset = incr(offset);

  fps_delay(15);
}

void breath_test() {
  static bool isFading;

  uint8_t r = get_led(0).r;
  
  if (isFading) {
    fade(max(1, 32 - r / 8));
  }
  else {
    glow(CRGB::Red, max(1, r / 12));
  }
  
  if (r >= 192) {
    isFading = true;
  }
  else if (r <= 32) {
    isFading = false;
  }
  
  fps_delay(30);
}

#endif // TESTS_H
