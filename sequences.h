#ifndef SEQUENCES_H
#define SEQUENCES_H

// lighting sequence type definitions
typedef void (*SequenceFunction)();
typedef SequenceFunction SequenceList[];

struct state_t {
  uint8_t frame;
  uint8_t hue;
  struct stripe_t {
    uint16_t offset;
  } stripe;
  struct loading_t {
    uint8_t width;
  } loading;
  struct corruption_t {
    uint8_t amount;
    uint8_t count;
    uint8_t stage;
  } corruption;
} state;

void cycle(const SequenceFunction *list, const int count, const long duration) {
  for (int i = 0; i < count; ++i) {
    const long start = millis();

    while (millis() - start < duration) {
      list[i]();
    }
  }
}

void randomize(const SequenceFunction *list, const int count, const long duration) {
  const SequenceFunction seq = list[random(count)];
  
  const long start = millis();

  while (millis() - start < duration) {
    seq();
  }
}

void stripe(const uint8_t width, uint8_t interval = 0, const struct CRGB & color = 0) {
  fill_solid(color ? color : CRGB::White, width, state.stripe.offset);

  state.stripe.offset = incr(state.stripe.offset, ++state.frame, interval);
}

void rainbow_stripe(const uint8_t width, const struct CHSV & hsv, uint8_t interval = 0, const struct CRGB & color = 0) {
  fill_rainbow(hsv);

  stripe(width, interval, color);
}

void rainbow_stripe(const uint8_t width, uint8_t interval = 0, const struct CRGB & color = 0, uint8_t sat = 192, uint8_t val = 192) {
  rainbow_stripe(width, CHSV(state.hue++, sat, val), interval, color);
}

uint8_t corruption(const uint8_t interval = 1) {
  state.corruption.amount = incr(state.corruption.amount, ++state.frame, interval, 255);
  uint16_t corrupted = scale16by8(NUM_LEDS, state.corruption.amount);

  while (corrupted > 0) {
    uint16_t index = random(NUM_LEDS);

    if (get_led(index)) {
      set_led(index, random(256) < 128 ? CRGB::Black : CRGB(random() >> 8));
      --corrupted;
    }
  }

  return state.corruption.amount;
}

void stripe_seq() {
  const uint8_t width = 3;

  clear();
  
  stripe(width);
  
  fps_delay(60);
}

void rainbow_stripe_seq() {
  const uint8_t width = 3;
  
  rainbow_stripe(width, 2);
  
  fps_delay(60);
}

void rainbow_strip_loading_seq() {
  const uint8_t stripe_width = 3;
  const uint8_t interval = NUM_LEDS * 2;

  clear();

  if (state.loading.width > 0) {
    fill_rainbow(state.hue++, state.loading.width, NUM_LEDS - state.loading.width);
  }

  stripe(stripe_width);

  // 180 frames / 60 fps = 3 seconds per add * 60 leds = 180 seconds = 3 mins
  state.loading.width = incr(state.loading.width, (state.frame = (state.frame + 1) % interval), interval);

  fps_delay(60);
}

void rainbow_stripe_corrupt_simp_seq() {
  const uint8_t width = 3, corruptionInterval = 16, maxCorruption = 192;
  
  rainbow_stripe(width, 2);

  // 16 interval / 192 max =~ 1min
  uint8_t amount = corruption(corruptionInterval);

  if (amount >= maxCorruption) {
    state.corruption.count = 0;
    LOG("stage"); LOG(state.corruption.stage++); LOG(" complete", true);
  }

  state.frame++;

  fps_delay(60);
}

// TODO: reach certain corruption level then hold
// split off core dump into its own sequence
void rainbow_stripe_corrupt_seq() {
  #define STAGE_INIT 0
  #define STAGE_CORRUPTION STAGE_INIT + 1
  #define STAGE_ERROR STAGE_CORRUPTION + 1
  #define STAGE_ERROR_WAIT STAGE_ERROR + 1
  #define STAGE_CORE_DUMP STAGE_ERROR_WAIT + 1
  #define STAGE_CORE_DUMP_WAIT STAGE_CORE_DUMP + 1
  #define STAGE_POWER_DOWN STAGE_CORE_DUMP_WAIT + 1

  const uint8_t width = 3, s0len = 1, s2len = 5, corruptionInterval = 16, maxCorruption = 192, interval = NUM_LEDS;
//  static uint8_t frame, count, stage;

  if (state.corruption.stage == STAGE_INIT) {
    rainbow_stripe(width, 2);
    
    if (state.corruption.count >= s0len) {
      state.corruption.count = 0;
      LOG("stage"); LOG(state.corruption.stage++); LOG(" complete", true);
    }
  }
  else if (state.corruption.stage == STAGE_CORRUPTION) {
    rainbow_stripe(width, 2);

    // 16 interval / 192 max =~ 1min
    uint8_t amount = corruption(corruptionInterval);

    if (amount >= maxCorruption) {
      state.corruption.count = 0;
      LOG("stage"); LOG(state.corruption.stage++); LOG(" complete", true);
    }
  }
  else if (state.corruption.stage == STAGE_ERROR) {
    for (int i = 0; i < NUM_LEDS / 8; ++i) {
      set_led(random(NUM_LEDS) + 1 % NUM_LEDS, CRGB::Red);
    }

    state.corruption.count = 0;
    ++state.corruption.stage;
  }
  else if (state.corruption.stage == STAGE_ERROR_WAIT) {
    if (state.corruption.count >= 2) {
      set_led(0, 0);
      state.corruption.count = 0;
      LOG("stage"); LOG(state.corruption.stage++); LOG(" complete", true);
    }
  }
  else if (state.corruption.stage == STAGE_CORE_DUMP) {
    glow(CRGB::Red, max(1, get_led(0).r > 192 ? 4 : (get_led(0).r + 32) / 32));

    if (get_led(0).r == 255) {
      state.corruption.count = 0;
      LOG("stage"); LOG(state.corruption.stage++); LOG(" complete", true);
    }
  }
  else if (state.corruption.stage == STAGE_CORE_DUMP_WAIT) {
    if (state.corruption.count >= 1) {
      state.corruption.count = 0;
      LOG("stage"); LOG(state.corruption.stage++); LOG(" complete", true);
    }
  }
  else if (state.corruption.stage == STAGE_POWER_DOWN) {
    fade(max(1, get_led(0).r < 64 ? 4 : (get_led(0).r - 32) / 32));

    if (get_led(0).r == 0) {
      state.corruption.count = 0;
      LOG("stage"); LOG(state.corruption.stage++); LOG(" complete", true);

      clear();
    }
  }

  state.corruption.count = incr(state.corruption.count, (state.frame = (state.frame + 1)% interval), interval);
  
  fps_delay();
}

#endif // SEQUENCES_H

