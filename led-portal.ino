// enable debugging functions
//#define DEBUG

// uncomment to use GRBW strips
#define __NEO_GRBW__
// uncomment to enable the accurate RGBW conversion function
// NOTE: it can cause flickering with certain sequences
//#define __NEO_GRBW_ACCURATE_CONVERSION__

// PWM output pin
#define LED_PIN 6

// RF Input Pins
#define PIN_A 5
#define PIN_B 4
#define PIN_C 3
#define PIN_D 2

// strip length
#define NUM_LEDS 30

// overall brightness
#define LED_BRIGHTNESS 128

// Target FPS
#define TARGET_FPS 120

// include the framework
#include "portal.h"

// include portal sequences
#include "sequences.h"

#ifdef DEBUG
// include the test sequences
#include "tests.h"
#endif

uint8_t currentPinState;

void setup() {
  delay(1000);

#ifdef DEBUG
  Serial.begin(9600);
#endif

  pinMode(PIN_A, INPUT);
  pinMode(PIN_B, INPUT);
  pinMode(PIN_C, INPUT);
  pinMode(PIN_D, INPUT);

  pinMode(LED_BUILTIN, OUTPUT);

#ifdef __NEO_GRBW__
  leds = Adafruit_NeoPixel(NUM_LEDS, LED_PIN, NEO_GRBW + NEO_KHZ800);
  leds.setBrightness(LED_BRIGHTNESS);
  leds.begin();
#else
  FastLED.addLeds<NEOPIXEL, LED_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(LED_BRIGHTNESS);
#endif

  currentPinState = 0;
  reset();
}

#ifdef DEBUG
SequenceList list = {
  rgb_test,                   // 0
  hue_test,                   // 1
  hsv_test,                   // 2
  fill_solid_range_test,      // 3
  fill_rainbow_range_test,    // 4
  fill_range_test,            // 5
  mask_test,                  // 6
  invmask_test,               // 7
  breath_test,                // 8
  stripe_seq,                 // 9
  rainbow_stripe_seq,         // 10
  rainbow_strip_loading_seq,  // 11
  rainbow_stripe_corrupt_seq, // 12
};
#endif

void buttonMonitor() {
  bool pin_A_pressed = digitalRead(PIN_A) == HIGH;
  bool pin_B_pressed = digitalRead(PIN_B) == HIGH;
  bool pin_C_pressed = digitalRead(PIN_C) == HIGH;
  bool pin_D_pressed = digitalRead(PIN_D) == HIGH;

  uint8_t pinState = (pin_A_pressed ? 1 : 0) + (pin_B_pressed ? 2 : 0) + (pin_C_pressed ? 4 : 0) + (pin_D_pressed ? 8 : 0);

  uint8_t m = (digitalRead(2) << 0) | (digitalRead(3) << 1) | (digitalRead(4) << 2) | (digitalRead(5) << 3);

  if (pinState != currentPinState) {
    currentPinState = pinState;
    led_resume();
    state.loading.width = 0;
    clear();

    LOG(currentPinState, true);
  }
}

void loop() {
  buttonMonitor();
  
  if (is_led_processing_enabled()) {
    switch(currentPinState) {
#ifdef DEBUG
      case 1:
        list[12]();
        break;
      case 2:
        list[3]();
        break;
      case 4:
        list[4]();
        break;
      case 8:
        list[10]();
        break;
      default:
        list[0]();
        break;
#else
      case 1:
        rainbow_strip_loading_seq();
        break;
      case 2:
        rainbow_stripe_seq();
        break;
      case 4:
        rainbow_stripe_corrupt_simp_seq();
        break;
      case 8:
        fill_solid(CRGB::Black);
        fps_delay();
        break;
#endif
    }
  }
}

