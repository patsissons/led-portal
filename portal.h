#ifndef PORTAL_H
#define PORTAL_H

// we need this regardless because we use some of the utility functions for both frameworks
#include <FastLED.h>

#ifdef __NEO_GRBW__
#include <Adafruit_NeoPixel.h>
#endif

#ifdef DEBUG
inline void LOG() {
  Serial.println();
}

inline void LOG(const char * text, bool newline = false) {
  if (newline) {
    Serial.println(text);
  }
  else {
    Serial.print(text);
  }
}

inline void LOG(const uint32_t value, bool newline = false, const uint8_t format = DEC) {
  if (newline) {
    Serial.println(value, format);
  }
  else {
    Serial.print(value, format);
  }
}

void dumpRGB(const struct CRGB & rgb, const bool newline = false) {
  LOG("(");
  LOG(rgb.r);
  LOG(", ");
  LOG(rgb.g);
  LOG(", ");
  LOG(rgb.b);
  LOG(")");

  if (newline) {
    LOG();
  }
}

void dumpHSV(const struct CHSV & hsv, const bool newline = false) {
  LOG("(");
  LOG(hsv.h);
  LOG(", ");
  LOG(hsv.s);
  LOG(", ");
  LOG(hsv.v);
  LOG(")");

  if (newline) {
    LOG();
  }
}

void dumpHSVRGB(const struct CHSV & hsv, const bool newline = false) {
  dumpRGB(CRGB(hsv), false);
  LOG("\t");
  dumpHSV(hsv, newline);
}
#else
#define LOG(...)
#define dumpRGB(rgb, newline)
#define dumpHSV(hsv, newline)
#define dumpHSVRGB(hsv, newline)
#endif

#ifdef __NEO_GRBW__
Adafruit_NeoPixel leds;

// we need a way to convert RGB to RGBW
// this function pointer allows us to choose a strategy based requirements
typedef uint32_t (*RGBW_CONVERTER)(const struct CRGB & input);

// this strategy will set the white channel to the provided parameter constant
uint32_t CONSTANT_RGB_RGBW(const struct CRGB & input, const uint8_t w) {
  return input ? leds.Color(input.r, input.g, input.b, w) : 0;
}

// this strategy will simply set the white channel to zero
uint32_t TRUNCATE_RGB_RGBW(const struct CRGB & input) {
  return CONSTANT_RGB_RGBW(input, 0);
}

// this strategy will approximate the white channel as the 
// intensity of the least intense RGB channel
uint32_t FAST_RGB_RGBW(const struct CRGB & input) {
  if (input) {
    const uint8_t w = min(input.r, min(input.g, input.b));
    return leds.Color(input.r, input.g, input.b, w);
  }
  else {
    return 0;
  }
}

#ifdef __NEO_GRBW_ACCURATE_CONVERSION__
// this strategy uses color theory to approximate perceived luminosity
// see: http://stackoverflow.com/a/40318604/2789877
uint32_t ACCURATE_RGB_RGBW(const struct CRGB & input) {
  if (input) {
    uint8_t Mi = max(input.r, max(input.g, input.b));
  
    if (Mi == 0) {
      return 0;
    }
  
    float multiplier = 255.0f / Mi;
    float hR = input.r * multiplier;
    float hG = input.g * multiplier;
    float hB = input.b * multiplier;  
  
    float M = max(hR, max(hG, hB));
    float m = min(hR, min(hG, hB));
    uint8_t L = ((M + m) / 2 - 127.5f) * 2 / multiplier;
  
    CRGB output = CRGB(input.r - L, input.g - L, input.b - L);
  
    return leds.Color(output.r, output.g, output.b, L);
  }
  else {
    return 0;
  }
}
#else
#define ACCURATE_RGB_RGBW FAST_RGB_RGBW
#endif

// this is our default conversion function
RGBW_CONVERTER RGB_RGBW = ACCURATE_RGB_RGBW;
#else
CRGBArray<NUM_LEDS> leds;
#endif

uint32_t incr(const uint32_t val, uint32_t frame = 0, uint32_t interval = 0, uint32_t maxVal = 0, uint32_t increment = 1) {
  return (interval == 0) ?
    (val + increment) % (maxVal == 0 ? NUM_LEDS : maxVal) :
    (frame % interval == 0) ? incr(val, 0, 0, maxVal, increment) : val;
}

CRGB get_led(const uint16_t index) {
#ifdef __NEO_GRBW__
  return CRGB(leds.getPixelColor(index) & 0xFFFFFF);
#else
  return leds[index];
#endif
}

void set_led(const uint16_t index, const struct CRGB & color) {
#ifdef __NEO_GRBW__
  leds.setPixelColor(index, RGB_RGBW(color));
#else
  leds[index] = color;
#endif
}

inline uint32_t limit(const uint32_t value, const uint32_t maxValue, const uint32_t minValue = 0) {
  return min(maxValue, max(minValue, value));
}

inline uint16_t get_width(uint16_t width) {
  return (width == 0 || width > NUM_LEDS) ? NUM_LEDS : width;
}

// smart fill_solid function
void fill_solid(const struct CRGB & color, uint16_t width = 0, const uint16_t offset = 0) {
  width = get_width(width);

  for (uint16_t i = 0; i < width; ++i) {
    set_led((offset + i) % NUM_LEDS, color);
  }
}

// smart fill_rainbow function
void fill_rainbow(const struct CHSV & hsv, uint16_t width = 0, const uint16_t offset = 0, const uint8_t dhue = 5) {
  CHSV color(hsv);
  
  width = get_width(width);

  for (uint16_t i = 0; i < width; ++i) {
    set_led((offset + i) % NUM_LEDS, color);
    color.hue += dhue;
  }
}

// smart fill_rainbow function (using 8 bit HSV values)
void fill_rainbow(const uint8_t hue, uint16_t width = 0, const uint16_t offset = 0, const uint8_t dhue = 5, uint8_t saturation = 240, uint8_t intensity = 255) {
  fill_rainbow(CHSV(hue, saturation, intensity), width, offset, dhue);
}

void fill_range(const CRGBSet & buffer, const uint16_t offset = 0) {
  for (uint16_t i = 0; i < buffer.size(); ++i) {
    set_led((offset + i) % NUM_LEDS, buffer[i]);
  }
}

void mask(const uint16_t width, const uint16_t offset = 0, const struct CRGB & color = CRGB::Black) {
  for (uint16_t i = width; i < NUM_LEDS; ++i) {
    set_led((offset + i) % NUM_LEDS, color);
  }
}

void invmask(const uint16_t width, const uint16_t offset = 0, const struct CRGB & color = CRGB::Black) {
  for (uint16_t i = 0; i < width; ++i) {
    set_led((offset + i) % NUM_LEDS, color);
  }
}

void glow(const struct CRGB & color, fract8 amount, uint16_t width = 0, const uint16_t offset = 0) {
  CRGB cadd = CRGB(color).nscale8(amount), csub = (-CRGB(color)).nscale8(amount);
  
  width = get_width(width);

  for (uint16_t i = 0; i < width; ++i) {
    uint16_t index = (offset + i) % NUM_LEDS;
    set_led(index, (get_led(index) + cadd) - csub);
  }
}

void fade(fract8 amount, uint16_t width = 0, const uint16_t offset = 0) {
  width = get_width(width);

  for (uint16_t i = 0; i < width; ++i) {
    uint16_t index = (offset + i) % NUM_LEDS;
    set_led(index, get_led(index).nscale8(255 - amount));
  }
}

long led_delay_marker;
void led_delay(const uint16_t wait = 0) { 
  if (wait > 0) {
    led_delay_marker = millis() + wait;
  }
}

void led_resume() {
  led_delay_marker = millis();
}

bool is_led_processing_enabled() {
  return millis() >= led_delay_marker;
}

// emit color channels and possibly delay
void show(const uint16_t wait = 0) {
#ifdef __NEO_GRBW__
  leds.show();
#else
  FastLED.show();
#endif
  
  led_delay(wait);
}

// delay for target FPS (or global target fps)
void fps_delay(const uint8_t fps = 0) {
  show();

  led_delay(1000 / (fps == 0 ? TARGET_FPS : fps));
}

void clear(const struct CRGB & color = CRGB::Black) {
  fill_solid(color);
}

// reset all lights to off
void reset(const struct CRGB & color = CRGB::Black) {
  clear(color);
  show();
}

#endif // PORTAL_H
